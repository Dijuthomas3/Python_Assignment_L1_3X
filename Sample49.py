#Write a program to perform  following file operations
#a) Open the file in read mode and read all its contents on to STDOUT.
#b) Open the file in write mode and enter 5 new lines of strings in to the new file.
#c) Open file in Append mode and add 5 lines of text into it.

import os

fo = open("python4.txt", "r")
read= fo.read(10);
print "Read String is : ", read

fo = open("python4.txt", "w")
write=fo.write( "Python is a great language.\n its intersting to learn\n this is my program\n we are creating it to modify\nmy team is good enough to help me\n");
fo = open("python4.txt", "r")
pos=fo.tell()
print pos
read= fo.read(200);
print "content in file is  : ", read
fo.close()

fo = open("python4.txt", "a")
wf=fo.write( "Connections can bring you ideas\n advice and even jobs\n on LinkedIn, the fastest way to find people is syncing your email\n Don't worry\n no invites are sent without your approval\n");
po=fo.seek(1,100)
fo = open("python4.txt", "r")
read= fo.read(300)
fo.close()
print 'appended the content in file please open the file and see it:',read

