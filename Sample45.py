# Write a program to check given string is Palindrome or not. (Using Function)

def palindrome(var1):
	'Check for palindrome'

 	rev=var1[::-1]
	print "Reverse of given input: ",rev
	if (rev==var1):
   	 	print "The given string is palindrome"
	else:
    		print "The given string is not palindrome"

var1=raw_input("Enter a string: ")
palindrome(var1)	
