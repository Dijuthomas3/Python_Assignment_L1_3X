#Using calendar module perform following operations.
#    a)print the 2016 calendar with space between months as 10 character.
#     b) How many leap days between the years 1980 to 2025.
#     c) Check given year is leap year or not.
#     d) print calendar of any specified month of the year 2016.


import calendar

calender=calendar.calendar(2016,w=2,l=1,c=10)
print "calender is:" ,calender

leapdays=calendar.leapdays(1980,2025)
print "leapdays between 1980 and 2025 is:",leapdays

curentyear=calendar.isleap(2017)
print "current year is leap year :",curentyear


year = int(input("Input the year : "))
month = int(input("Input the month : "))
print "specified month of the year 2016:",(calendar.month(year, month))
