#Write a program with lambda function to perform following.
 #  a) Perform all the operations of basic calculator ( add, sub, multiply, divide, modulus, floor division )

a=input('enter a:')
b=input('enter b:')

sum = lambda a, b: a + b;
sub = lambda a, b: a - b;
mul = lambda a, b: a * b;
divide = lambda a, b: a / b;
modulus = lambda a, b: a % b;
floor = lambda a, b: a // b;

print "Value of  sum : ", sum(a,b)
print "Value of  sub: ", sub(a,b)
print "Value of  mul: ", mul(a,b)
print "Value of  division: ", divide(a,b)
print "Value of  modulo: ", modulus(a,b)
print "Value of  floor division : ", floor(a,b)
