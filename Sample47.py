# Write function to extend the tuple with elements of list. Pass list and Tuple as parameter to the function. 

def append(list1,tup1):
	'Extend tuple with elements of list'
	list2=list(tup1)
	n=len(list2)
	for i in range(n):
		list1.append(list2[i])
	tup2=tuple(list1)
	print "The final tuple after adding list elements: ", tup2
	return
list1=input("Enter the values in list:")
tup1=input("Enter the tuple values: ")
append(list1,tup1)
