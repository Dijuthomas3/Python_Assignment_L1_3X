#Write a function to find the biggest of 4 numbers.
# a) All numbers are passed as arguments separately ( Required argument)
# b) use default values for arguments ( Default arguments) 

def biggest(arg1,*args):
	'Finding biggest of 4 numbers'
	a=args[0];b=args[1];c=args[2];d=args[3]
	
	if(a>b and a>c and a>d):
    		print "the biggest number is: ", a
	elif(b>a and b>c and b>d):
    		print "the biggest number is: ", b
	elif(c>a and c>b and c>d):
    		print "the biggest number is: ", c
	else:
    		print "the biggest number is: ", d
	return

args=(3,5,8,6)
biggest(1,*args)
